from http.server import BaseHTTPRequestHandler
from http.server import ThreadingHTTPServer
import json


SUPPORTED_PROTOCOL_VERSIONS = [
    '0.1.0'
]


def map_data(protocol_version, data):
    '''
    Maps input data depending on protocol version
    '''
    if protocol_version == '0.1.0':
        return data['flat']
    else:
        raise ValueError('Unknown protocol version %s' % protocol_version)


class ServerHandler(BaseHTTPRequestHandler):
    def __init__(self, protocol_version, registry, *args, **kwargs):
        self._protocol_version = protocol_version
        self._registry = registry
        super(ServerHandler, self).__init__(*args, **kwargs)

    def send_content_type(self):
        self.send_header('Content-Type', 'application/json')

    def send_json_error(self, code, msg):
        self.send_response(400)
        self.send_content_type()
        self.end_headers()
        self.wfile.write(bytes('{"error": "%s"}' % msg, 'utf-8'))

    def bad_request(self, msg):
        self.send_json_error(400, msg)

    def not_found(self, msg):
        self.send_json_error(404, msg)

    def internal_error(self, msg):
        self.send_json_error(500, msg)

    def ok(self, value):
        self.send_response(200)
        self.send_content_type()
        self.end_headers()
        self.wfile.write(bytes(json.dumps({'value': value}), 'utf-8'))

    def do_POST(self):
        # It could have charset specified
        if not self.headers.get('content-type', '').startswith('application/json'):
            self.bad_request('Expecting json in request body')
            return
        if 'X-Protocol-Version' not in self.headers:
            self.bad_request('Missing protocol version in headers')
            return
        elif self.headers['X-Protocol-Version'] != self._protocol_version:
            self.bad_request('App is expecting protocol version v%s. Got v%s instead.' %
                             (self._protocol_version, self.headers['X-Protocol-Version']))
            return

        # Resource lookup
        try:
            handle = self._registry[self.path]
        except KeyError:
            self.not_found('Resource %s not found' % self.path)
            return

        # Read content
        read = self.rfile.read(int(self.headers['Content-Length']))

        # Try to load request body as json
        try:
            data = json.loads(read)
        except json.decoder.JSONDecodeError as ex:
            self.bad_request(ex.msg)
            return

        # It should always be a known version since App() ensures it
        # but error is tested for consistency
        try:
            data = map_data(self._protocol_version, data)
        except Exception as ex:
            self.internal_error(ex)
            return

        # Compute result and issue response
        try:
            result = handle(data)
            self.ok(result)
        except Exception as ex:
            self.internal_error('Could not calculate result: %s' % ex)


class App:
    _registry = dict()

    def __init__(self, *, protocol_version, host='localhost', port=8080):
        if protocol_version not in SUPPORTED_PROTOCOL_VERSIONS:
            raise ValueError('Unknown protocol version: %s' % protocol_version)
        # Builder pattern
        self._protocol_version = protocol_version
        self._host = host
        self._port = port

    def register_handle(self, path, handle):
        self._registry[path] = handle

    def start(self):
        ThreadingHTTPServer((self._host, self._port),
                            lambda *args, **kwargs:
                                ServerHandler(self._protocol_version,
                                              self._registry,
                                              *args,
                                              **kwargs))\
            .serve_forever()


class FirstOrDefault:
    def __init__(self, default):
        self._default = default

    def __call__(self, data):
        if len(data) > 0:
            return data[0]['val']
        return self._default


def avg(data):
    return sum(doc['val'] for doc in data) / len(data)


def rand(data):
    from random import choice
    return choice(data)['val']


if __name__ == '__main__':
    # Testing
    app = App(protocol_version='0.1.0')
    app.register_handle('/', FirstOrDefault(42))
    app.register_handle('/avg', avg)
    app.register_handle('/rand', rand)
    app.start()

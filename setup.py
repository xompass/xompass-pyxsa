import setuptools


def readme():
    with open('README.md') as f:
        return f.read()


setuptools.setup(
    name='pyxsa',
    version='0.1.0',
    author='Rodolfo Castillo Mateluna',
    author_email='rodolfo.castillo@xompass.com',
    description='Python bindings for Xbus Stream Analysis',
    long_description=readme(),
    url='https://bitbucket.org/xompass/xompass-pyxsa.git',
    packages=setuptools.find_packages()
)
